package com.sinalefa.grabilitychallenge;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.test.InstrumentationTestCase;
import android.view.View;
import android.widget.AbsListView;
import android.widget.RadioButton;

import com.sinalefa.grabilitychallenge.controllers.MoviesFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.Robolectric;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by danie on 07/07/2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, application = PopMovies.class)
public class TVTest {

    private static final String FRAGMENT_TAG = "fragment";
    private MainActivity activity;
    private MoviesFragment movieList;
    private FragmentManager manager;

    /**
     * Add fragment to activity
     */
    @Before
    public void setUp() throws Exception {
        movieList = new MoviesFragment();
        activity = Robolectric.buildActivity(MainActivity.class).create().get();
        manager = activity.getSupportFragmentManager();
        movieList.setType("movie/");
        manager.beginTransaction().add(movieList, FRAGMENT_TAG).commit();
    }

    @Test
    public void testMovie() throws Exception {
        assertNotNull(activity);
        assertNotNull(activity.findViewById(R.id.btnPop));
        assertNotNull(activity.findViewById(R.id.btnTop));
        assertNotNull(activity.findViewById(R.id.btnUpcoming));
        assertEquals("On Air title is different!", "On Air", ((RadioButton) activity.findViewById(R.id.btnUpcoming)).getText().toString());

    }
}