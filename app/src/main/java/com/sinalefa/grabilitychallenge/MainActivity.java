package com.sinalefa.grabilitychallenge;

import android.app.SearchManager;
import android.content.Context;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.sinalefa.grabilitychallenge.controllers.MoviesFragment;
import com.sinalefa.grabilitychallenge.controllers.SearchViewFragment;
import com.sinalefa.grabilitychallenge.models.SearchModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CancellationException;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchViewFragment.OnListFragmentInteractionListener {
    private String query;
    private SearchViewOnQueryTextListener searchViewOnQueryTextListener = new SearchViewOnQueryTextListener();
    private DateFormat dateFormat;
    private HttpURLConnection conn;
    private JSONAsyncTask request;
    private SearchViewFragment searchView = new SearchViewFragment();
    private MoviesFragment moviesList = new MoviesFragment();
    private SearchView searchMenu;
    private MenuItem searchViewItem;
    private SVItemExpand onSearchViewItemExpand = new SVItemExpand();
    private boolean searchViewTap;
    private FragmentManager fm;
    private int numerito;
    @Override
    public void onListFragmentInteraction(SearchModel item) {

    }

    /**
     * Class which listens when we tap on the SearchView.
     */
    public class SVItemExpand implements MenuItemCompat.OnActionExpandListener {
        FragmentManager fm = getSupportFragmentManager();

        /**
         * Called when the searchView icon is tapped.
         *
         * @param item our searchView;
         * @return true if the item should expand, false if expansion should be suppressed.
         * If we have navigated through items from the search view result list and we tap again,
         * we clear our backStack.
         */
        @Override
        public boolean onMenuItemActionExpand(MenuItem item) {

            // search view key
            searchViewTap = true;
            FragmentTransaction transaction = fm.beginTransaction();
            searchView.setTitle(getResources().getString(R.string.search_title));
            transaction.replace(R.id.frame_container, searchView);
            transaction.commit();
            return true;
        }

        /**
         * Called when click back button or by collapseSearchView() method.
         *
         * @param item our searchView;
         * @return true if the item should collapse, false if collapsing should be suppressed.
         */
        @Override
        public boolean onMenuItemActionCollapse(MenuItem item) {
            return true;
        }
    }
    /**
     * Class which listens for SearchView events.
     */
    private class SearchViewOnQueryTextListener implements SearchView.OnQueryTextListener {

        /**
         * Called when the query text is changed by the user.
         *
         * @param newText the new content of the query text field.
         * @return false if the SearchView should perform the default action of showing any suggestions if available,
         * true if the action was handled by the listener.
         */
        @Override
        public boolean onQueryTextChange(String newText) {
            query = newText;
            query = query.replaceAll("[\\s%\"^#<>{}\\\\|`]", "%20");

            if (query.length() > 1) {
                new Thread(new Runnable() {
                    public void run() {
                        try {

                            if (request != null)
                                request.cancel(true);

                            if (conn != null)
                                conn.disconnect();

                            request = new JSONAsyncTask();
                            request.execute(PopMovies.url + "search/multi?query=" + query + "?&api_key=" + PopMovies.key);
                            request.setQuery(query);
                        } catch (CancellationException e) {
                            if (request != null)
                                request.cancel(true);
                            // we abort the http request, else it will cause problems and slow connection later
                            if (conn != null)
                                conn.disconnect();
                        }
                    }
                }).start();
            }
            return true;
        }


        /**
         * Called when the user submits the query.
         * This could be due to a key press on the keyboard or due to pressing a submit button.
         *
         * @param query the query text that is to be submitted
         * @return true if the query has been handled by the listener, false to let the SearchView perform the default action.
         * We update the query, hide the keyboard and add the query to the suggestions.
         */
        @Override
        public boolean onQueryTextSubmit(String query) {
            searchView.setQuery(query);
            searchMenu.clearFocus();
            return true;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        dateFormat = android.text.format.DateFormat.getDateFormat(this);
        fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        moviesList.setType("movie/");
        transaction.replace(R.id.frame_container, moviesList);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        searchViewItem = menu.findItem(R.id.search);
        searchMenu = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchMenu.setQueryHint(getResources().getString(R.string.search_hint));
        searchMenu.setOnQueryTextListener(searchViewOnQueryTextListener);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchViewItemC =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchViewItemC.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));


        MenuItemCompat.setOnActionExpandListener(searchViewItem, onSearchViewItemExpand);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_movies) {
            fm = getSupportFragmentManager();
            FragmentTransaction transaction = fm.beginTransaction();
            moviesList.setType("movie/");
            transaction.replace(R.id.frame_container, moviesList);
            transaction.commit();
            moviesList.updateList();
        } else if (id == R.id.nav_series) {
            fm = getSupportFragmentManager();
            FragmentTransaction transaction = fm.beginTransaction();
            moviesList.setType("tv/");
            transaction.replace(R.id.frame_container, moviesList);
            transaction.commit();
            moviesList.updateList();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    /**
     * This class handles the connection to our backend server.
     * If the connection is successful we set our list data.
     */
    class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {
        private ArrayList<Integer> idsList;
        private ArrayList<String> posterPathList;
        private String queryZ;

        public void setQuery(String query) {
            this.queryZ = query;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Boolean doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(10000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();

                int status = conn.getResponseCode();
                if (status == 200) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    br.close();

                    JSONObject searchData = new JSONObject(sb.toString());
                    JSONArray searchResultsArray = searchData.getJSONArray("results");
                    int length = searchResultsArray.length();
                    if (length > 10)
                        length = 10;

                    idsList = new ArrayList<>();
                    posterPathList = new ArrayList<>();
                    for (int i = 0; i < length; i++) {
                        JSONObject object = searchResultsArray.getJSONObject(i);

                        int id = 0;
                        String title = "", posterPath = "", releaseDate = "", mediaType = "";


                        if (object.has("id") && object.getInt("id") != 0)
                            id = object.getInt("id");

                        if (object.has("title"))
                            title = object.getString("title");

                        if (object.has("name"))
                            title = object.getString("name");
                        title = title.replaceAll("'", "");

                        if (object.has("poster_path") && !object.getString("poster_path").equals("null") && !object.getString("poster_path").isEmpty())
                            posterPath = PopMovies.imageUrl + "w154" + object.getString("poster_path");


                        if (object.has("profile_path") && !object.getString("profile_path").equals("null") && !object.getString("profile_path").isEmpty())
                            posterPath = PopMovies.imageUrl + "w154" + object.getString("profile_path");

                        if (object.has("release_date") && !object.getString("release_date").equals("null") && !object.getString("release_date").isEmpty()) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                            try {
                                Date date = sdf.parse(object.getString("release_date"));
                                String formattedDate = dateFormat.format(date);
                                releaseDate = "(" + formattedDate + ")";
                            } catch (java.text.ParseException e) {
                            }
                        }

                        if (object.has("first_air_date") && !object.getString("first_air_date").equals("null") && !object.getString("first_air_date").isEmpty()) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                            try {
                                Date date = sdf.parse(object.getString("first_air_date"));
                                String formattedDate = dateFormat.format(date);
                                releaseDate = "(" + formattedDate + ")";
                            } catch (java.text.ParseException e) {
                            }
                        }

                        if (object.has("media_type") && !object.getString("media_type").isEmpty())
                            mediaType = object.getString("media_type");

                    }

                    return true;
                }


            } catch (ParseException | IOException | JSONException e) {
                if (conn != null)
                    conn.disconnect();
            } finally {
                if (conn != null)
                    conn.disconnect();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (query.length() > 1) {


                if (posterPathList != null && posterPathList.size() > 0) {
                    for (int i = 0; i < posterPathList.size(); i++) {
//                        searchImgLoadingListener = new SearchImgLoadingListener(idsList.get(i), queryZ);
//                        imageLoader.loadImage(posterPathList.get(i), searchImgLoadingListener);
                    }
                }
            }


        }

    }
    public void showView(final View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.VISIBLE);
            }
        });
    }
    public void hideView(final View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.GONE);
            }
        });
    }
}
