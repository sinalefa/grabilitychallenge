package com.sinalefa.grabilitychallenge.controllers;


import android.content.Intent;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

import com.sinalefa.grabilitychallenge.MainActivity;
import com.sinalefa.grabilitychallenge.PopMovies;
import com.sinalefa.grabilitychallenge.R;
import com.sinalefa.grabilitychallenge.adapters.MovieAdapter;
import com.sinalefa.grabilitychallenge.adapters.SearchAdapter;
import com.sinalefa.grabilitychallenge.models.MovieModel;
import com.sinalefa.grabilitychallenge.persistance.PopMoviesDatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private ListView listView;
    private ArrayList<MovieModel> moviesList;
    private MovieAdapter movieAdapter;
    private EndlessScrollListener endlessScrollListener;
    private HttpURLConnection conn;
    private Toast toastLoadingMore;
    private MovieModel movieModel;
    private View rootView;
    private MainActivity activity;
    private ProgressBar spinner;
    private int totalPages;
    private int checkLoadMore;
    private boolean isLoading;
    private int backState;
    private String currentList;
    private RadioButton btnPop, btnTop, btnUpcoming,btnCheckOffline;
    private Button btnOffline;
    private String type;
    private PopMoviesDatabaseHelper databaseHelper;
    public void updateList() {
        if (getActivity() != null) {
            listView = (ListView) getActivity().findViewById(R.id.list_movies);
            moviesList = new ArrayList<>();
            movieAdapter = new MovieAdapter(getActivity(), R.layout.row, moviesList);
            listView.setAdapter(movieAdapter);
            endlessScrollListener = new EndlessScrollListener();
            listView.setOnScrollListener(endlessScrollListener);
            listView.setOnItemClickListener(this);
            final JSONAsyncTask request = new JSONAsyncTask();
            new Thread(new Runnable() {
                public void run() {
                    try {
                        System.out.println(PopMovies.url+getType() + getCurrentList() + "?&api_key=" + PopMovies.key);
                        request.execute(PopMovies.url+getType() + getCurrentList() + "?&api_key=" + PopMovies.key).get(10000, TimeUnit.MILLISECONDS);
                    } catch (TimeoutException | ExecutionException | InterruptedException e) {
                        request.cancel(true);
                        // we abort the http request, else it will cause problems and slow connection later
                        if (conn != null)
                            conn.disconnect();
                        toastLoadingMore.cancel();
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getActivity(), getResources().getString(R.string.timeout), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }
            }).start();
        }
    }
    public MoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_movies, container, false);
        movieModel = new MovieModel();
        activity = ((MainActivity) getActivity());
        toastLoadingMore = Toast.makeText(activity, R.string.loadingMore, Toast.LENGTH_SHORT);
        spinner = (ProgressBar) rootView.findViewById(R.id.progressbar);
        currentList = "popular";
        btnPop = (RadioButton) rootView.findViewById(R.id.btnPop);
        btnPop.setOnClickListener(this);
        btnTop = (RadioButton) rootView.findViewById(R.id.btnTop);
        btnTop.setOnClickListener(this);
        btnUpcoming = (RadioButton) rootView.findViewById(R.id.btnUpcoming);
        btnUpcoming.setOnClickListener(this);
        btnOffline = (Button) rootView.findViewById(R.id.btnOffline);
        btnOffline.setOnClickListener(this);
        btnCheckOffline = (RadioButton) rootView.findViewById(R.id.btnCheckOffline);
        btnCheckOffline.setOnClickListener(this);
        databaseHelper = PopMoviesDatabaseHelper.getInstance(getContext());
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateList();
    }

    public String getCurrentList() {
        return currentList;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnTop){
            currentList="top_rated";
            updateList();
        }
        if(v.getId()==R.id.btnPop){
            currentList="popular";
            updateList();
        }if(v.getId()==R.id.btnUpcoming){
            if(type.equals("movie/")){
                currentList="upcoming";
            }else{
                currentList="on_the_air";
            }
            updateList();
        }if(v.getId()==R.id.btnOffline){
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    for (int i =0; i<moviesList.size();i++){
                        databaseHelper.addMovie(moviesList.get(i));
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    Toast.makeText(getActivity(), getResources().getString(R.string.saved_offline), Toast.LENGTH_SHORT).show();
                }
            }.execute();
        }
        if(v.getId()==R.id.btnCheckOffline){
            moviesList = databaseHelper.getAllMovies();
            listView.setAdapter(null);
            listView.setAdapter(new MovieAdapter(getActivity(), R.layout.row, moviesList));

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent i = new Intent(getContext(),DetailsActivity.class);
        i.putExtra("movie",moviesList.get(position));
        startActivity(i);
    }

    /**
     * This class listens for scroll events on the list.
     */
    public class EndlessScrollListener implements ListView.OnScrollListener {

        private int currentPage = 1;
        private boolean loading = false;
        private int oldCount = 0;

        public EndlessScrollListener() {
        }


        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

            if (oldCount != totalItemCount && firstVisibleItem + visibleItemCount >= totalItemCount) {
                loading = true;
                oldCount = totalItemCount;
            }
            if (loading) {
                if (currentPage != totalPages) {
                    currentPage++;
                    checkLoadMore = 1;
                    loading = false;
                    final JSONAsyncTask request = new JSONAsyncTask();
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                request.execute(PopMovies.url +getType() +getCurrentList() + "?&api_key=" + PopMovies.key + "&page=" + currentPage).get(10000, TimeUnit.MILLISECONDS);
                            } catch (TimeoutException | ExecutionException | InterruptedException e) {
                                request.cancel(true);
                                // we abort the http request, else it will cause problems and slow connection later
                                if (conn != null)
                                    conn.disconnect();
                                toastLoadingMore.cancel();
                                currentPage--;
                                loading = true;
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            Toast.makeText(getActivity(), getResources().getString(R.string.timeout), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        }
                    }).start();
                } else {
                    if (totalPages != 1) {
                        Toast.makeText(getActivity(), R.string.nomoreresults, Toast.LENGTH_SHORT).show();
                    }
                    loading = false;

                }
            }


        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getOldCount() {
            return oldCount;
        }

        public void setOldCount(int oldCount) {
            this.oldCount = oldCount;
        }

        public boolean getLoading() {
            return loading;
        }

        public void setLoading(boolean loading) {
            this.loading = loading;
        }

    }
    class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (checkLoadMore == 0) {
                activity.showView(spinner);
                isLoading = true;
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        toastLoadingMore.show();
                    }
                });
            }

        }

        @Override
        protected Boolean doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(10000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();

                int status = conn.getResponseCode();
                if (status == 200) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    br.close();

                    JSONObject movieData = new JSONObject(sb.toString());
                    totalPages = movieData.getInt("total_pages");
                    JSONArray movieArray = movieData.getJSONArray("results");

                    // is added checks if we are still on the same view, if we don't do this check the program will crash
                    if (isAdded()) {
                        if(type.equals("movie/")){
                            for (int i = 0; i < movieArray.length(); i++) {
                                JSONObject object = movieArray.getJSONObject(i);

                                MovieModel movie = new MovieModel();
                                movie.setId(object.getInt("id"));
                                movie.setTitle(object.getString("title"));
                                if (!object.getString("release_date").equals("null") && !object.getString("release_date").isEmpty())
                                    movie.setReleaseDate(object.getString("release_date"));


                                if (!object.getString("poster_path").equals("null") && !object.getString("poster_path").isEmpty())
                                    movie.setPosterPath(PopMovies.imageUrl + getResources().getString(R.string.imageSize) + object.getString("poster_path"));
                                if (!object.getString("overview").equals("null") && !object.getString("overview").isEmpty())
                                    movie.setOverview(object.getString("overview"));


                                moviesList.add(movie);
                            }
                        }
                        else{
                            for (int i = 0; i < movieArray.length(); i++) {
                                JSONObject object = movieArray.getJSONObject(i);

                                MovieModel movie = new MovieModel();
                                movie.setId(object.getInt("id"));
                                movie.setTitle(object.getString("name"));
                                if (!object.getString("first_air_date").equals("null") && !object.getString("first_air_date").isEmpty())
                                    movie.setReleaseDate(object.getString("first_air_date"));


                                if (!object.getString("poster_path").equals("null") && !object.getString("poster_path").isEmpty())
                                    movie.setPosterPath(PopMovies.imageUrl + getResources().getString(R.string.imageSize) + object.getString("poster_path"));
                                if (!object.getString("overview").equals("null") && !object.getString("overview").isEmpty())
                                    movie.setOverview(object.getString("overview"));

                                moviesList.add(movie);
                            }
                        }


                        return true;
                    }

                }


            } catch (ParseException | IOException | JSONException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                if (conn != null)
                    conn.disconnect();
            } finally {
                if (conn != null)
                    conn.disconnect();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            // is added checks if we are still on the same view, if we don't do this check the program will cra
            if (isAdded()) {
                if (checkLoadMore == 0) {
                    activity.hideView(spinner);
                    isLoading = false;
                }

                if (!result) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.noConnection), Toast.LENGTH_LONG).show();
                    backState = 0;
                } else {
                    movieAdapter.notifyDataSetChanged();
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            toastLoadingMore.cancel();
                        }
                    });


                    backState = 1;
//                    save = null;
                }
            }
        }

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        if(btnUpcoming!=null){
        if(type.equals("tv/")){
            btnUpcoming.setText("On Air");
        }else{
            btnUpcoming.setText("Upcoming");
        }

        }
    }
}
