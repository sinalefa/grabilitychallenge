package com.sinalefa.grabilitychallenge.controllers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.sinalefa.grabilitychallenge.PopMovies;
import com.sinalefa.grabilitychallenge.R;
import com.sinalefa.grabilitychallenge.models.MovieModel;
import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {
    private MovieModel actualMovie;
    private TextView titleText;
    private TextView releaseDate;
    private ImageView posterPath;
    private TextView tagline;
    private TextView statusText;
    private TextView runtime;
    private TextView genres;
    private TextView countries;
    private TextView companies;
    private TextView overview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actualMovie = getIntent().getExtras().getParcelable("movie");

        titleText = (TextView) findViewById(R.id.title);
        releaseDate = (TextView) findViewById(R.id.releaseDate);
        posterPath = (ImageView) findViewById(R.id.posterPath);
        tagline = (TextView) findViewById(R.id.tagline);
        statusText = (TextView) findViewById(R.id.status);
        runtime = (TextView) findViewById(R.id.runtime);
        genres = (TextView) findViewById(R.id.genres);
        countries = (TextView) findViewById(R.id.countries);
        companies = (TextView) findViewById(R.id.companies);
        overview = (TextView) findViewById(R.id.overview);
        titleText.setText(actualMovie.getTitle());
        Picasso.with(this).load(actualMovie.getPosterPath()).into(posterPath);
        releaseDate.setText(actualMovie.getReleaseDate());
        genres.setText(actualMovie.getCharacter());
        companies.setText(actualMovie.getDepartmentAndJob());
        overview.setText(actualMovie.getOverview());
    }

}
