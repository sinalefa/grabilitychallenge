package com.sinalefa.grabilitychallenge.controllers;

import android.content.Context;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.sinalefa.grabilitychallenge.MainActivity;
import com.sinalefa.grabilitychallenge.PopMovies;
import com.sinalefa.grabilitychallenge.R;
import com.sinalefa.grabilitychallenge.adapters.SearchAdapter;
import com.sinalefa.grabilitychallenge.models.SearchModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class SearchViewFragment extends Fragment implements AdapterView.OnItemClickListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private String title;
    private String query;
    private ListView listView;
    private ArrayList searchList;
    private SearchAdapter searchAdapter;
    private int totalPages;
    private EndlessScrollListener endlessScrollListener;
    private HttpURLConnection conn;
    private int backState;
    private Toast toastLoadingMore;
    private MainActivity activity;

    /**
     * Makes a new request to the server with the given query.
     */
    public void search() {
        System.out.println("Try query");
        if (getActivity() != null) {
            System.out.println("Actividad no nula");
            listView = (ListView) getActivity().findViewById(R.id.list);
            searchList = new ArrayList<>();
            searchAdapter = new SearchAdapter(getActivity(), R.layout.row, searchList);
            listView.setAdapter(searchAdapter);
            endlessScrollListener = new EndlessScrollListener();
            listView.setOnScrollListener(endlessScrollListener);
            final JSONAsyncTask request = new JSONAsyncTask();
            new Thread(new Runnable() {
                public void run() {
                    try {
                        System.out.println("Manda ejecución");
                        request.execute(PopMovies.url + "search/multi?query=" + getQuery() + "?&api_key=" + PopMovies.key).get(10000, TimeUnit.MILLISECONDS);
                    } catch (TimeoutException | ExecutionException | InterruptedException e) {
                        request.cancel(true);
                        // we abort the http request, else it will cause problems and slow connection later
                        if (conn != null)
                            conn.disconnect();
                        toastLoadingMore.cancel();
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getActivity(), getResources().getString(R.string.timeout), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }
            }).start();
        }
    }

    /**
     * Se activa al buscar
     *
     * @param query the query to search
     */
    public void setQuery(String query) {
        this.query = query.replaceAll("[\\s%\"^#<>{}\\\\|`]", "%20");
        search();
    }
    public String getQuery(){
        return  query;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    /**
     * This class handles the connection to our backend server.
     * If the connection is successful we set list results.
     */
    class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    toastLoadingMore.show();
                }
            });
        }

        @Override
        protected Boolean doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(10000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();

                int status = conn.getResponseCode();
                if (status == 200) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    br.close();

                    JSONObject searchData = new JSONObject(sb.toString());
                    totalPages = searchData.getInt("total_pages");
                    JSONArray searchResultsArray = searchData.getJSONArray("results");

                    for (int i = 0; i < searchResultsArray.length(); i++) {
                        JSONObject object = searchResultsArray.getJSONObject(i);

                        SearchModel movie = new SearchModel();
                        if (object.has("id") && object.getInt("id") != 0)
                            movie.setId(object.getInt("id"));

                        if (object.has("title"))
                            movie.setTitle(object.getString("title"));

                        if (object.has("name")) {
                            if (object.has("media_type") && object.getString("media_type").equals("tv"))
                                movie.setTitle(object.getString("name"));
                            else
                                movie.setTitle(object.getString("name"));
                        }
                        if (object.has("release_date") && !object.getString("release_date").equals("null") && !object.getString("release_date").isEmpty())
                            movie.setReleaseDate(object.getString("release_date"));

                        if (object.has("first_air_date") && !object.getString("first_air_date").equals("null") && !object.getString("first_air_date").isEmpty())
                            movie.setReleaseDate(object.getString("first_air_date"));

                        // is added checks if we are still on the same view, if we don't do this check the program will crash
                        if (isAdded()) {
                            if (object.has("poster_path") && !object.getString("poster_path").equals("null") && !object.getString("poster_path").isEmpty())
                                movie.setPosterPath(PopMovies.imageUrl + getResources().getString(R.string.imageSize) + object.getString("poster_path"));
                        }

                        if (isAdded()) {
                            if (object.has("profile_path") && !object.getString("profile_path").equals("null") && !object.getString("profile_path").isEmpty())
                                movie.setPosterPath(PopMovies.imageUrl + getResources().getString(R.string.imageSize) + object.getString("profile_path"));
                        }

                        if (object.has("media_type") && !object.getString("media_type").isEmpty())
                            movie.setMediaType(object.getString("media_type"));

                        searchList.add(movie);
                    }

                    return true;
                }


            } catch (ParseException | IOException | JSONException e) {
                if (conn != null)
                    conn.disconnect();
            } finally {
                if (conn != null)
                    conn.disconnect();
            }
            return false;
        }

        protected void onPostExecute(Boolean result) {
            // is added checks if we are still on the same view, if we don't do this check the program will cra
            System.out.println("Pasa por acasito");
            if (isAdded()) {
                if (!result) {
                    Toast.makeText(getActivity(), R.string.noConnection, Toast.LENGTH_LONG).show();
                    backState = 0;
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            toastLoadingMore.cancel();
                        }
                    });
                    backState = 1;
                    if (!searchList.isEmpty())
                        searchAdapter.notifyDataSetChanged();
                    else
                        Toast.makeText(getActivity(), R.string.noResults, Toast.LENGTH_LONG).show();

                }
            }
        }

    }

    /**
     * This class listens for scroll events on the list.
     */
    public class EndlessScrollListener implements AbsListView.OnScrollListener {

        private int currentPage = 1;
        private boolean loading = false;
        private int oldCount = 0;

        public EndlessScrollListener() {
        }


        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

            if (oldCount != totalItemCount && firstVisibleItem + visibleItemCount >= totalItemCount) {
                loading = true;
                oldCount = totalItemCount;
            }
            if (loading) {
                if (currentPage != totalPages) {
                    currentPage++;
                    loading = false;
                    final JSONAsyncTask request = new JSONAsyncTask();
                    new Thread(new Runnable() {
                        public void run() {
                            try {
                                request.execute(PopMovies.url + "search/multi?query=" + getQuery() + "?&api_key=" + PopMovies.key + "&page=" + currentPage).get(10000, TimeUnit.MILLISECONDS);
                            } catch (TimeoutException | ExecutionException | InterruptedException e) {
                                request.cancel(true);
                                // we abort the http request, else it will cause problems and slow connection later
                                if (conn != null)
                                    conn.disconnect();
                                toastLoadingMore.cancel();
                                currentPage--;
                                loading = true;
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            Toast.makeText(getActivity(), getResources().getString(R.string.timeout), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        }
                    }).start();

                } else {
                    if (totalPages != 1) {
                        Toast.makeText(getActivity(), R.string.nomoreresults, Toast.LENGTH_SHORT).show();
                    }
                    loading = false;

                }
            }


        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getOldCount() {
            return oldCount;
        }

        public void setOldCount(int oldCount) {
            this.oldCount = oldCount;
        }

        public boolean getLoading() {
            return loading;
        }

        public void setLoading(boolean loading) {
            this.loading = loading;
        }
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SearchViewFragment() {
    }
    /**
     * Update the title. We use this method to save our title and then to set it on the Toolbar.
     */
    public void setTitle(String title) {
        this.title = title;
    }
    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static SearchViewFragment newInstance(int columnCount) {
        SearchViewFragment fragment = new SearchViewFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_searchview_list, container, false);
        listView = (ListView) view.findViewById(R.id.list);
        listView.setOnItemClickListener(this);
        toastLoadingMore = Toast.makeText(getActivity(), R.string.loadingMore, Toast.LENGTH_SHORT);
        activity = ((MainActivity) getActivity());

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(SearchModel item);
    }
}
