package com.sinalefa.grabilitychallenge.persistance;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sinalefa.grabilitychallenge.models.MovieModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danie on 06/07/2017.
 */

public class PopMoviesDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DATABASELOG";
    private static PopMoviesDatabaseHelper sInstance;
    // Database Info
    private static final String DATABASE_NAME = "movieDatabase";
    private static final int DATABASE_VERSION = 2;


    // Table Names
    private static final String TABLE_MOVIE = "movie";
    private static final String TABLE_TV = "tv";

    // Movie Table Columns
    private static final String KEY_MOVIE_ID = "id";

    private static final String KEY_MOVIE_TITLE = "title";
    private static final String KEY_MOVIE_RELEASE_DATE = "release";
    private static final String KEY_MOVIE_POSTER = "poster";
    private static final String KEY_MOVIE_CHARACTER= "character";
    private static final String KEY_MOVIE_DEP= "dep";
    private static final String KEY_MOVIE_MEDIA= "media";
    private static final String KEY_MOVIE_OVERVIEW= "overview";


    // Called when the database connection is being configured.
    // Configure database settings for things like foreign key support, write-ahead logging, etc.
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    // Called when the database is created for the FIRST time.
    // If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MOVIE_TABLE = "CREATE TABLE " + TABLE_MOVIE +
                "(" +
                KEY_MOVIE_ID + " INTEGER PRIMARY KEY," + // Define a primary key
                KEY_MOVIE_TITLE + " TEXT," +
                KEY_MOVIE_RELEASE_DATE + " TEXT," +
                KEY_MOVIE_POSTER + " TEXT," +
                KEY_MOVIE_CHARACTER + " TEXT," +
                KEY_MOVIE_DEP + " TEXT," +
                KEY_MOVIE_MEDIA + " TEXT," +
                KEY_MOVIE_OVERVIEW + " TEXT"+
                ")";

        String CREATE_TV_TABLE = "CREATE TABLE " + TABLE_TV +
                "(" +
                KEY_MOVIE_ID + " INTEGER PRIMARY KEY," + // Define a primary key
                KEY_MOVIE_TITLE + " TEXT," +
                KEY_MOVIE_RELEASE_DATE + " TEXT," +
                KEY_MOVIE_POSTER + " TEXT," +
                KEY_MOVIE_CHARACTER + " TEXT," +
                KEY_MOVIE_DEP + " TEXT," +
                KEY_MOVIE_MEDIA + " TEXT," +
                KEY_MOVIE_OVERVIEW + " TEXT"+
                ")";

        db.execSQL(CREATE_MOVIE_TABLE);
        db.execSQL(CREATE_TV_TABLE);
    }

    // Called when the database needs to be upgraded.
    // This method will only be called if a database already exists on disk with the same DATABASE_NAME,
    // but the DATABASE_VERSION is different than the version of the database that exists on disk.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            // Simplest implementation is to drop all old tables and recreate them
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOVIE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_TV);
            onCreate(db);
        }
    }
    public static synchronized PopMoviesDatabaseHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new PopMoviesDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    /**
     * Constructor should be private to prevent direct instantiation.
     * Make a call to the static method "getInstance()" instead.
     */
    private PopMoviesDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Query Movies
     * @return
     */
    public ArrayList<MovieModel> getAllMovies() {
        ArrayList<MovieModel> movies = new ArrayList<>();

        // SELECT * FROM POSTS
        // LEFT OUTER JOIN USERS
        // ON POSTS.KEY_POST_USER_ID_FK = USERS.KEY_USER_ID
        String MOVIE_QUERY =
                String.format("SELECT * FROM %s ",
                        TABLE_MOVIE);

        // "getReadableDatabase()" and "getWriteableDatabase()" return the same object (except under low
        // disk space scenarios)
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(MOVIE_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    MovieModel newMovie = new MovieModel();
                    newMovie.setId( cursor.getInt(cursor.getColumnIndex(KEY_MOVIE_ID)));
                    newMovie.setTitle(cursor.getString(cursor.getColumnIndex(KEY_MOVIE_TITLE)));
                    newMovie.setReleaseDate(cursor.getString(cursor.getColumnIndex(KEY_MOVIE_RELEASE_DATE)));
                    newMovie.setPosterPath(cursor.getString(cursor.getColumnIndex(KEY_MOVIE_POSTER)));
                    newMovie.setCharacter(cursor.getString(cursor.getColumnIndex(KEY_MOVIE_CHARACTER)));
                    newMovie.setDepartmentAndJob(cursor.getString(cursor.getColumnIndex(KEY_MOVIE_DEP)));
                    newMovie.setMediaType(cursor.getString(cursor.getColumnIndex(KEY_MOVIE_MEDIA)));
                    newMovie.setOverview(cursor.getString(cursor.getColumnIndex(KEY_MOVIE_OVERVIEW)));

                    movies.add(newMovie);
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to get populations from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return movies;
    }


    // Insert or update a Movie in the database
    // Since SQLite doesn't support "upsert" we need to fall back on an attempt to UPDATE (in case the
    // population already exists) optionally followed by an INSERT (in case the user does not already exist).
    // Unfortunately, there is a bug with the insertOnConflict method
    // (https://code.google.com/p/android/issues/detail?id=13045) so we need to fall back to the more
    // verbose option of querying for the user's primary key if we did an update.
    public long addMovie(MovieModel movie) {
        // The database connection is cached so it's not expensive to call getWriteableDatabase() multiple times.
        SQLiteDatabase db = getWritableDatabase();
        long movieId = -1;

        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_MOVIE_ID, movie.getId());
            values.put(KEY_MOVIE_TITLE, movie.getTitle());
            values.put(KEY_MOVIE_RELEASE_DATE, movie.getReleaseDate());
            values.put(KEY_MOVIE_POSTER, movie.getPosterPath() );
            values.put(KEY_MOVIE_CHARACTER, movie.getCharacter() );
            values.put(KEY_MOVIE_DEP, movie.getDepartmentAndJob() );
            values.put(KEY_MOVIE_MEDIA,movie.getMediaType());
            values.put(KEY_MOVIE_OVERVIEW,movie.getOverview());
            db.insertOrThrow(TABLE_MOVIE, null, values);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add or update población");
        } finally {
            db.endTransaction();
        }
        return movieId;
    }
    public String deleteMovie(String idMovie)
    {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from  " + TABLE_MOVIE +" where id=\'" + idMovie+"\'" );
        return idMovie;
    }
    // Insert or update a Movie in the database
    // Since SQLite doesn't support "upsert" we need to fall back on an attempt to UPDATE (in case the
    // population already exists) optionally followed by an INSERT (in case the user does not already exist).
    // Unfortunately, there is a bug with the insertOnConflict method
    // (https://code.google.com/p/android/issues/detail?id=13045) so we need to fall back to the more
    // verbose option of querying for the user's primary key if we did an update.
    public long addTV(MovieModel movie) {
        // The database connection is cached so it's not expensive to call getWriteableDatabase() multiple times.
        SQLiteDatabase db = getWritableDatabase();
        long movieId = -1;

        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_MOVIE_ID, movie.getId());
            values.put(KEY_MOVIE_TITLE, movie.getTitle());
            values.put(KEY_MOVIE_RELEASE_DATE, movie.getReleaseDate());
            values.put(KEY_MOVIE_POSTER, movie.getPosterPath() );
            values.put(KEY_MOVIE_CHARACTER, movie.getCharacter() );
            values.put(KEY_MOVIE_DEP, movie.getDepartmentAndJob() );
            values.put(KEY_MOVIE_MEDIA,movie.getMediaType());
            values.put(KEY_MOVIE_OVERVIEW,movie.getOverview());
            db.insertOrThrow(TABLE_TV, null, values);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            Log.d(TAG, "Error while trying to add or update población");
        } finally {
            db.endTransaction();
        }
        return movieId;
    }
    public String deleteTV(String idMovie)
    {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("delete from  " + TABLE_TV +" where id=\'" + idMovie+"\'" );
        return idMovie;
    }
    public void deleteAll() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            // Order of deletions is important when foreign key relationships exist.
            db.delete(TABLE_MOVIE, null, null);
            db.delete(TABLE_TV, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG, "Error while trying to delete all posts and users");
        } finally {
            db.endTransaction();
        }
    }
}
